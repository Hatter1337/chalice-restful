import re
from datetime import datetime

import trafaret as t
from passlib.hash import argon2

from chalicelib.config import config


class Password(t.String):
    """ String field that hashes the password using argon2. """

    def check_and_return(self, value):
        min_length = 6
        message = "must contain at least 6 characters without spaces"

        if " " in value or len(value) < min_length:
            self._failure(message)
        return argon2.hash(value)


class Name(t.String):
    """ The string must not be empty or contain only spaces. """

    def check_and_return(self, value):
        if not (value and value.strip()):
            self._failure("value must not be empty")
        return value

    def __repr__(self):
        return "<Name>"


class DateTime(t.String):
    """ Validate string date format. """

    def check_and_return(self, value):
        try:
            datetime.strptime(value, config["datetime_format"])
        except ValueError:
            raise self._failure("incorrect date format, "
                                "should be {}".format(config["datetime_format"]))
        return value

    def __repr__(self):
        return "<DateTime>"


class BirthDate(t.String):
    """ Validate string date format. Date must not be greater than current. """

    def check_and_return(self, value):
        try:
            dt = datetime.strptime(value[:10], config["date_format"])
        except ValueError:
            raise self._failure("incorrect date format, "
                                "should be {}".format(config["date_format"]))

        if dt > datetime.now():
            raise self._failure("date must not be greater than current")
        return value[:10]

    def __repr__(self):
        return "<BirthDate>"


class PhoneNumber(t.String):
    """ Field for phone numbers. """

    def check_and_return(self, value):
        # Regular Expressions Cookbook, 2nd Edition by Steven Levithan, Jan Goyvaerts
        validator = re.compile("^\+?(?:[0-9] ?){6,14}[0-9]$")
        message = "not a valid phone number, please use format: +country_code numbers"

        if not validator.search(value):
            self._failure(message)

        # add (+) if needed (international phone numbers include a leading plus sign)
        value = "+" + value if value[0] != "+" else value
        # delete a space characters
        value = value.replace(" ", "")
        return value

    def __repr__(self):
        return "<PhoneNumber>"
