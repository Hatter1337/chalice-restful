class HttpException(Exception):
    """ Base class for HTTP exceptions

        :param status_code (int): HTTP status
        :param response_body (dict): API response for this error

    """
    status_code = 500
    response_body = None

    def response(self):
        return self.status_code, self.response_body


class ResourceNotFoundException(HttpException):
    """ Exception for HTTP status code = 404 (Resource not found). """
    status_code = 404
    response_body = None


class InvalidParametersException(HttpException):
    """ Exception for HTTP status code = 400 (Invalid Parameters).

        :param errors: Dictionary of errors for each parameter.

    """

    status_code = 400

    def __init__(self, errors: dict):
        self.response_body = errors


class AccessDeniedException(HttpException):
    """ Exception for HTTP status code = 401 (Access Denied). """

    status_code = 401

    def __init__(self, message=None):
        message = message or "You do not have permission to access this resource"
        self.response_body = {"unauthorized": message}


class NotModifiedException(HttpException):
    """ Exception for HTTP status code = 304 (Not Modified). """

    status_code = 304
    response_body = None


class NoContentException(HttpException):
    """ Exception for HTTP status code = 204 (No Content). """

    status_code = 204
    response_body = None
