import json

import trafaret as t
from trafaret import dataerror

from chalice import Response
from chalicelib.rest.http_exceptions import InvalidParametersException


def build_response(status_code: int,
                   body=None,
                   response_headers: dict = None) -> Response:
    """ The function creates the object Response.

        :param status_code: response status as integer;
        :param body: response body;
        :param response_headers: additional response headers;

        :return: chalice.Response object.

    """

    headers = {
        "Content-Type": "application/json",
    }

    body = json.dumps(body) if isinstance(body, (dict, list)) else body

    if response_headers and isinstance(response_headers, dict):
        headers.update(response_headers)

    return Response(body, status_code=status_code, headers=headers)


def param_validate(schema, param):
    """ Validates the parameter according to the specified scheme. """

    try:
        return schema.check(param)
    except t.dataerror.DataError as error:
        raise InvalidParametersException(
            {
                key: "Parameter '{}' - {}".format(key, value)
                for key, value in error.as_dict().items()
            }
        )
