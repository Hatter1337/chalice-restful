import hashlib
import json
import traceback

import trafaret as t

from chalicelib import logger
from chalicelib.rest.helpers import build_response, param_validate
from chalicelib.rest.http_exceptions import InvalidParametersException, HttpException


# singleton object that represents parameters not supplied
_missing = object()


class URLMap(dict):
    _base_url = None

    def build_url(self, endpoint, **values):
        """ Returns a URL for the given endpoint. Placeholder values are
            filled in by the keyword arguments.

            :param endpoint: The endpoint name to get a URL for;
            :type endpoint: str;

            :param values: Keyword arguments for the placeholder values in the URL;
                absolute (bool): Make the URL absolute;

            :return: (str) url.

        """

        absolute = values.pop("_absolute", False)
        path = self[endpoint].format(**values)
        if absolute:
            return "{}{}".format(self._base_url, path)
        else:
            return path

    def set_base_url_from_request(self, request):
        """ Sets the base URL so that absolute URLs can be requested. """

        headers = request.headers
        context = request.context
        proto = headers.get("x-forwarded-proto", "http")

        scheme_and_host = "{}://{}".format(proto, headers["host"])
        stage = context.get("stage")

        count_rp = context.get("resourcePath", "").count("/")
        count_path = context.get("path", "").count("/")

        if count_rp != count_path:
            self._base_url = "/".join(filter(None, [scheme_and_host, stage]))
        else:
            self._base_url = scheme_and_host


url_map = URLMap()
""" Mapping of endpoint names to URL paths. """


class Context:
    """ Context object with extra, helpful accessors. Based on _AppCtxGlobals from Flask. """

    def get(self, name, default=None):
        return self.__dict__.get(name, default)

    def pop(self, name, default=_missing):
        if default is _missing:
            return self.__dict__.pop(name)
        else:
            return self.__dict__.pop(name, default)

    def setdefault(self, name, default=None):
        return self.__dict__.setdefault(name, default)

    def __contains__(self, item):
        return item in self.__dict__

    def __iter__(self):
        return iter(self.__dict__)


class Rest:
    """ Rest.

        :param http_verbs (list(str)): Supported HTTP methods.
        :param app (Chalice): The chalice application object.
        :param g (Context): Request user context.

    """

    http_verbs = ["get", "post", "put", "patch", "delete"]

    def __init__(self, chalice_app):
        self.app = chalice_app
        self.g = None
        self._first_request = True
        self._before_request_handlers = []
        self._http_error_handlers = {}
        self._teardown_request_handlers = []

    def add_resource(self, resource, path, endpoint_name, **kwargs):
        """ Adds routes for a resource. Functions on the resource object that
            match the HTTP verb are turned into a Chalice route.

            :param resource: The resource class to build routes from.
            :type resource: Resource;

            :param path: The URL path to access this resource.
            :type path: str;

            :param endpoint_name: Name for the resource. The name of the
               route in Chalice is the HTTP verb concatenated with the resource name.
            :type endpoint_name: str;

            :param kwargs: Other parameters that get passed to Chalice.route().

        """

        url_map[endpoint_name] = path

        for verb in self.http_verbs:
            if hasattr(resource, verb):
                method = getattr(resource, verb)
                content_types = getattr(method, "_route_content_types", None)
                if content_types:
                    kwargs["content_types"] = content_types
                self._add_route(resource, verb, path, **kwargs)

    def _add_route(self, resource, http_verb, path, **kwargs):
        def resource_method(**args):
            teardown_error = None
            self.g = Context()
            if self._first_request:
                self._first_request = False
                url_map.set_base_url_from_request(self.app.current_request)

            try:
                self._on_before_request()
                instance = resource(self.app.current_request)
                response = instance.dispatch(http_verb, args)

            except HttpException as error:
                response = error.response()
                self._on_http_error(error)
            except Exception as error:
                teardown_error = error
                tb = traceback.format_exc()
                code = hashlib.sha256(tb.encode("utf-8")).hexdigest()[:8]
                body = {
                    "message": "Internal Server Error",
                    "code": code
                }
                response = (500, body)
                logger.error("ERROR {}: {}".format(code, error))
                logger.error("TRACEBACK: {}".format(tb))
            finally:
                self._on_teardown_request(teardown_error)
                self.g = None

            return build_response(*response)

        self.app.route(path, methods=[http_verb.upper()], **kwargs)(resource_method)

    def before_request(self, func):
        """ Decorator that registers a function to be run before each request.
            Multiple functions can be registered and are run in order.
            The function is called without any arguments.
        """

        self._before_request_handlers.append(func)
        return func

    def _on_before_request(self):
        for handler in self._before_request_handlers:
            handler()

    def http_error_handler(self, status_code):
        """ Decorator that registers a function to be run when the HTTP
            response has the given status code. Only a single function can
            be registered. Any new function will replace the previously set function.

            :param status_code: HTTP response status code
            :type status_code: int.

        """

        def wrapper(func):
            self._http_error_handlers[status_code] = func
            return func
        return wrapper

    def _on_http_error(self, error):
        handler = self._http_error_handlers.get(error.status_code)
        if handler:
            handler(error)

    def teardown_request(self, func):
        """ Decorator that registers a function to be run at the end of each
            request. Multiple functions can be registered and are run in order.
            If an exception occurred while processing the request, it gets
            passed to each teardown_request function.
        """

        self._teardown_request_handlers.append(func)
        return func

    def _on_teardown_request(self, error):
        for handler in self._teardown_request_handlers:
            handler(error)

    @staticmethod
    def content_types(types):
        """ Decorator that sets the acceptable content types for this route.
            This is used to override any content type set for the resource for a specific method.

           :param types: List of the content types to accept.
           :type types: list(str).

        """

        def wrapper(func):
            func._route_content_types = types
            return func
        return wrapper


class Resource:
    """ Represents a RESTful resource. Concrete resources should extend from
        this class and expose methods for each supported HTTP method. A request
        will invoke the appropriate method on the resource and passed all
        arguments from the url rule used when adding the resource.

        :param auth (Authorizer): Class attribute that sets an Authorizer that is
            used to check the Authorization header for each method of this
            resource. Default is None;

        :param headers (dict): Request headers.

        :param request_body (bytes): The raw HTTP body.

        :param params (dict): Parsed parameters. If the request has a body and
            the Content-Type is json, then the parameters are the parsed
            JSON body. Otherwise, it would be the query parameters.

    """

    auth = None

    def __init__(self, request):
        logger.info("Call endpoint: {} {}".format(
            request.context.get("httpMethod"), request.context.get("path")))
        self.headers = request.headers
        self.request_body = request.raw_body

        if request.raw_body and self.headers.get("Content-Type") in [
                "application/json",
                "application/json-patch+json",
                "application/merge-patch+json"]:
            try:
                self.params = json.loads(request.raw_body)
            except json.JSONDecodeError:
                raise InvalidParametersException({"body": "Request body is not valid JSON"})
        else:
            self.params = request.query_params

    def dispatch(self, http_verb, kwargs):
        """ Dispatches the request to the appropriate method after authorizing the request.

        :return: tuple: status_code and response. If the response is a dict or list,
            it is serialized as JSON to be included in the response body.
            Other types are included in the response body as is.

        """

        if self.auth:
            self.auth().authorize(self.headers)

        method = getattr(self, http_verb)
        return method(**kwargs)

    def process_input(self, schema):
        """ Takes a Schema and loads the parameters through it.
            Validation errors will trigger an InvalidParametersException.

            :return: (dict) The processed parameters.

        """

        if not self.params:
            raise InvalidParametersException({"json": "no input data"})

        if not isinstance(schema, t.Trafaret):
            raise TypeError("Validator must be a 'trafaret' type")

        return param_validate(schema, self.params)

    @staticmethod
    def transform_db_objects(schema, data, many=False):
        """ Convert SQLAlchemy objects to dict or list(dict) with object attributes. """
        if not data:
            return [] if many else None

        allowed_keys = [key.name for key in schema.keys]

        if isinstance(data, list):
            objects = [{k: v for k, v in dict(obj).items() if k in allowed_keys} for obj in data]
        else:
            objects = {k: v for k, v in dict(data).items() if k in allowed_keys}
        return schema.check(objects)

    @staticmethod
    def url_for(endpoint, **values):
        """ Returns a url for the given endpoint with any placeholders
            filled in by the keyword arguments.

        Args:
            :param endpoint: Name of the endpoint.
            :type endpoint: str;

            :param values: Placeholder values.

            :return: (str) url.

        """

        return url_map.build_url(endpoint, **values)
