from abc import ABCMeta, abstractmethod
from datetime import datetime, timedelta, timezone

from authlib.specs import rfc7519 as jwt_lib
from authlib.specs.rfc7515 import errors

from chalicelib import logger
from chalicelib.rest import api
from chalicelib.config import config
from chalicelib.rest.http_exceptions import AccessDeniedException

SECRET_WORD = "Bearer"


def generate_token(body, expire_time=None, expire_value=None):
    """ Encodes a claims set and returns a JWT string.

        :param body: data for encrypt;
        :type body: dict;

        :param expire_time: token lifetime in hours;
        :type expire_time: int;

        :param expire_value: token expiration time;
        :type expire_value: datetime.datetime;

        :return: JSON Web Token.

    """

    body.update({
        "iss": config["jwt"]["auth_issuer"],
        "aud": config["jwt"]["auth_audience"]
    })

    if expire_value:
        body.update({"exp": expire_value})
    elif expire_time:
        body.update({"exp": datetime.utcnow() + timedelta(hours=expire_time)})

    token = jwt_lib.jwt.encode({"alg": "HS256"}, body, config["jwt"]["secret_key"])
    return token.decode("utf-8")


def verify_token(data, verify_exp=True):
    """ The function performs authentication using JWT.

        :param data: Bearer + JSON Web Token | JSON Web Token;

        :param verify_exp: Verify exception, default - True;
        :type verify_exp: bool;

        :return: (bool) token is verified, (dict) payload or None.

    """

    try:
        word_token = data.split(" ")
        word, token = word_token if len(word_token) == 2 else ("", data)

        if word == SECRET_WORD:
            body = jwt_lib.jwt.decode(
                token,
                config["jwt"]["secret_key"],
                claims_options={"aud": {"essential": True, "value": config["jwt"]["auth_audience"]},
                                "exp": {"essential": verify_exp}}
            )
            # body.validate() always validates everything.
            # To skip expiration, use a very small value for current time,
            # otherwise, use None so validate"s default is used.
            now = None if verify_exp else 1
            body.validate(now=now)
            return True, body
    except errors.DecodeError:
        pass
    except jwt_lib.JWTError as error:
        logger.info("AUTH-ERROR: {}".format(error.description))
    return False, None


def auth(auth_token, jwt_verify_exp):
    if auth_token:
        verified, body = verify_token(auth_token, verify_exp=jwt_verify_exp)
        if not verified:
            raise AccessDeniedException
        else:
            api.g.user_id = body.get("user_id")
    else:
        raise AccessDeniedException


def auth_response(user_id, expiration=24):
    token_exp = datetime.utcnow() + timedelta(hours=expiration)
    return {
        "expiresAt": int(token_exp.replace(tzinfo=timezone.utc).timestamp()),
        "token": generate_token(
            {"user_id": str(user_id), "type": "auth"}, expire_value=token_exp
        )
    }


class Auth(metaclass=ABCMeta):
    """ Base class for authorization. """

    @abstractmethod
    def authorize(self, headers):
        """ Call "auth" function. """


class UserAuthorizer(Auth):
    """ Authorizer for user access. """

    def authorize(self, headers):
        auth(headers.get("Authorization"), jwt_verify_exp=True)


class AuthRefresh(Auth):
    """ Refresh authorization for user access. """

    def authorize(self, headers):
        auth(headers.get("Authorization"), jwt_verify_exp=False)
