import os
import json
import base64

import boto3
import trafaret as t

from chalicelib.rest.helpers import param_validate

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
TEMPLATES_DIR = os.path.abspath(os.path.join(CURRENT_DIR, os.pardir)) + "/templates/email/"


class Postman:
    def __init__(self, recipient, subject, template_name, letter_args, region="eu-central-1"):
        self.lambda_client = boto3.client("lambda", region_name=region)
        self.recipient = self.check_recipient(recipient)
        self.subject = subject
        self.template_name = template_name
        self.letter_args = letter_args

    @staticmethod
    def render_template(name, letter_args):
        with open(TEMPLATES_DIR + "{}.html".format(name), 'r') as template:
            data = template.read()
            return data.format(**letter_args)

    @staticmethod
    def check_recipient(recipient):
        return param_validate(t.Email, recipient)

    def deliver(self):
        mail_info = {
            "custom": {
                "recipient": self.recipient,
                "subject": self.subject,
                "body": self.render_template(self.template_name, self.letter_args)
            }
        }

        self.lambda_client.invoke(
            FunctionName="postman",
            InvocationType="RequestResponse",
            ClientContext=base64.b64encode(str.encode(json.dumps(mail_info))).decode("utf-8")
        )
