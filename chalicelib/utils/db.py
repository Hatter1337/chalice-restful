from contextlib import contextmanager

from chalicelib.database import Session


@contextmanager
def session_scope(expire=True):
    """
    Provide a transactional scope around a series of operations.

    Args:
        expire: Defaults to True.
        When True, all instances will be fully expired after each commit().

    """
    session = Session(expire_on_commit=expire)
    try:
        yield session
        session.commit()
    except Exception as error:
        session.rollback()
        raise
    finally:
        session.close()
