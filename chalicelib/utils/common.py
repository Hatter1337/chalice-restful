from time import time

from chalicelib import logger


class Profiler(object):
    """
    Returns the execution time of the code, using context manager.

    Example:
        with Profiler() as p:
            two = 1 + 1
        # >> Elapsed time: 0.0000026226 sec.

    """
    def __enter__(self):
        self._startTime = time()

    def __exit__(self, tp, value, traceback):
        logger.info("Execution time: {:.10f} sec.".format(time() - self._startTime))
