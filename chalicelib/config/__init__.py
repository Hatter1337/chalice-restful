import os
import json


filename = os.path.join(os.path.dirname(__file__), "config.json")
with open(filename) as f:
    # TODO: os.environ.get("API_STAGE", "dev")
    config = json.load(f)["dev"]
