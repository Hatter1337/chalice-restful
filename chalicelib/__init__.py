import logging

from chalice import Chalice


app = Chalice(app_name="chalice-restful")

logger = logging.getLogger('chalice-restful')
