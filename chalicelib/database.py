from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database, drop_database

from chalicelib import logger
from chalicelib.models import Base
from chalicelib.config import config

DATABASE_URL = "postgresql+psycopg2://{user}:{password}@{host}:{port}/{name}".format(
    user=config["db"]["user"],
    password=config["db"]["password"],
    host=config["db"]["host"],
    port=config["db"]["port"],
    name=config["db"]["name"]
)

options = {
    "pool_recycle": 3600,
    "pool_size": 20,
    "pool_timeout": 60,
    "max_overflow": 60,
    "echo": config["db"]["echo"]
}
engine = create_engine(DATABASE_URL, **options)

Session = sessionmaker()
Session.configure(bind=engine)


def recreate_db():
    logger.info("Recreating database ...")
    if database_exists(DATABASE_URL):
        drop_database(DATABASE_URL)
        engine.dispose()
        create_database(DATABASE_URL)
    else:
        create_database(DATABASE_URL)
    Base.metadata.create_all(engine)
    logger.info("Created new database")


if __name__ == "__main__":
    # recreate_db()
    Base.metadata.create_all(engine)
