import enum
from datetime import datetime

from sqlalchemy import Column, BigInteger
from sqlalchemy.ext.declarative import declarative_base


def table_repr(class_name, instance, mapper):
    """ __repr__ method for table class.

        :param class_name: class name.
        :param instance: instance of the table class (self).
        :param mapper: self.__mapper__.

        :return: str object.

    """

    values = vars(instance)
    return "<%s(" % class_name + ", ".join([
        str(values[attr]) for attr in mapper.columns.keys()
        if attr in values
    ]) + ")>"


class BaseModel:
    """ Model: Base. """

    id = Column(BigInteger, primary_key=True, autoincrement=True)

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __iter__(self):
        values = vars(self)
        desired_values = getattr(self, "_iter_values", None)
        for attr in self.__mapper__.columns.keys():
            if attr in desired_values if desired_values else values:
                value = values[attr]
                if isinstance(value, datetime):
                    yield attr, str(value)
                elif isinstance(value, enum.Enum):
                    yield attr, value.name
                else:
                    yield attr, value

    def __repr__(self):
        return table_repr(str(__class__.__name__), self, self.__mapper__)

    def merge_patch(self, params):
        for key, value in params.items():
            setattr(self, key, value)


Base = declarative_base(cls=BaseModel)
