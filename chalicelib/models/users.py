from sqlalchemy import Column, String, Text, TIMESTAMP

from chalicelib.models.base import Base


class Users(Base):
    """ Model: Users. """

    __tablename__ = "users"

    email = Column(String(100), index=True, unique=True)
    password = Column(Text)
    name = Column(String(100))
    creation_date = Column(TIMESTAMP)
    last_login_date = Column(TIMESTAMP, nullable=True, default=None)
