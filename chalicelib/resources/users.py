import trafaret as t

from chalicelib.models import Users
from chalicelib.auth import UserAuthorizer
from chalicelib.rest import Resource, fields, api
from chalicelib.rest.http_exceptions import InvalidParametersException


class User(Resource):
    auth = UserAuthorizer

    schema_request = t.Dict({
        t.Key("email", optional=True): t.String,
        t.Key("password", optional=True): fields.Password,
        t.Key("name", optional=True): fields.Name
    })

    schema_response = t.Dict({
        t.Key("email"): t.String,
        t.Key("name"): t.String,
        t.Key("creation_date"): t.String,
        t.Key("last_login_date"): t.String | t.Null
    })

    def get(self):
        user = api.g.session.query(Users) \
            .filter(Users.id == api.g.user_id).first()

        if not user:
            raise InvalidParametersException({"logic": "The user does not exist"})

        return 200, self.transform_db_objects(self.schema_response, user)

    @api.content_types(["application/merge-patch+json"])
    def patch(self):
        params = self.process_input(self.schema_request)

        user = api.g.session.query(Users) \
            .filter(Users.id == api.g.user_id).first()

        if not user:
            raise InvalidParametersException({"logic": "The user does not exist"})

        if "email" in params:
            db_user = api.g.session.query(Users) \
                .filter(Users.email == params["email"]).first()

            if db_user and user != db_user:
                raise InvalidParametersException({"logic": "Email already exists"})

        user.merge_patch(params)
        api.g.session.commit()
        return 204, ''
