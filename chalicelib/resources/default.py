from chalicelib.rest import Resource


class Default(Resource):
    def get(self):
        return 200, {
            "signup": self.url_for("sign_up", _absolute=True),
            "signin": self.url_for("sign_in", _absolute=True)
        }
