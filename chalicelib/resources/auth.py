from datetime import datetime

import trafaret as t
from passlib.hash import argon2

from chalicelib.models import Users
from chalicelib.config import config
from chalicelib.utils.db import session_scope
from chalicelib.rest import Resource, fields, api
from chalicelib.auth import AuthRefresh, auth_response
from chalicelib.rest.http_exceptions import AccessDeniedException, InvalidParametersException


class SignUp(Resource):
    schema = t.Dict({
        t.Key("email"): t.Email,
        t.Key("password"): fields.Password,
        t.Key("name"): fields.Name
    })

    def post(self):
        params = self.process_input(self.schema)

        with session_scope(expire=False) as session:
            email_exists = session.query(Users) \
                .filter(Users.email == params["email"]).first()

            if email_exists:
                raise InvalidParametersException({"logic": "Email already exists"})

            params["creation_date"] = datetime.utcnow().strftime(config["datetime_format"])
            user = Users(**params)
            session.add(user)
            session.commit()

        body = auth_response(user.id)
        return 201, body


class SignIn(Resource):
    schema = t.Dict({
        t.Key("email"): t.Email,
        t.Key("password"): t.String
    })

    def post(self):
        params = self.process_input(self.schema)

        with session_scope(expire=False) as session:
            user = session.query(Users) \
                .filter(Users.email == params["email"]).first()

            if not user:
                raise AccessDeniedException("Email/password invalid")

            if not argon2.verify(params["password"], user.password):
                raise AccessDeniedException("Email/password invalid")

            user.last_login_date = datetime.utcnow().strftime(config["datetime_format"])
            session.commit()

            body = auth_response(user.id)
            return 200, body


class LoginRefreshToken(Resource):
    auth = AuthRefresh

    def get(self):
        body = auth_response(api.g.user_id)
        return 200, body
