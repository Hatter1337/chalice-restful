Chalice-RESTful
==============

Base framework: **[Chalice](https://github.com/aws/chalice)**

Special features:
--------------
* REST

Config:
--------------
That everything works correctly - you need to create an app config!

Add file "chalicelib/config/config.json":
```
{
  "dev": {
    "date_format": "%Y-%m-%d",
    "datetime_format": "%Y-%m-%d %H:%M:%S",
    "x-api-key": "",
    "host": "* Your Rest API URL (after first deploy)",
    "jwt": {
      "secret_key": "* 64 character string",
      "auth_issuer": "chalice-restful",
      "auth_audience": "chalice-restful-recipient"
    },
    "db": {
      "host": "* Your DB host",
      "port": "* Your DB port",
      "name": "* Your DB name",
      "user": "* Your DB user",
      "password": "* Your DB password",
      "echo": true
    }
  }
}
```

Also, do not forget to change the "iam_role_arn" value in file ".chalice/config.json" to your ARN role.