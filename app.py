from chalicelib import app, resources, log
from chalicelib.rest import api
from chalicelib.database import Session

log.setup_logging()


@api.before_request
def before_request():
    api.g.session = Session()


@api.http_error_handler(400)
def handle_400_error(error):
    api.g.session.rollback()
    return error.response()


@api.teardown_request
def teardown_request(error):
    if error:
        api.g.session.rollback()
    api.g.session.close()


# Default
api.add_resource(resources.Default, "/", "default")

# Auth
api.add_resource(resources.SignUp, "/signup", "sign_up", api_key_required=False)
api.add_resource(resources.SignIn, "/signin", "sign_in", api_key_required=False)
api.add_resource(resources.LoginRefreshToken, "/refresh", "refresh", api_key_required=False)

# User
api.add_resource(resources.User, "/user", "user", api_key_required=False)
